
let container = document.querySelector('.container');
let modalWrap = document.querySelector('.modal-wrap');
let filteredProducts = products;

document.querySelector('#asc').addEventListener('click', sortAsc);
document.querySelector('#desc').addEventListener('click', sortDesc);
document.querySelector('#rating').addEventListener('click', sortAsc);
document.querySelector('.close-modal').addEventListener('click', closeModal);
modalWrap.addEventListener('click', closeModal);
document.querySelector('.modal').addEventListener('click', stopProp);


function createTag(tagName) {
    let tagItem = document.createElement('div');
    tagItem.classList.add('tag-item');
    tagItem.innerText = tagName;
    tagItem.addEventListener('click', () => filterByTag(tagName));
    return tagItem;
}

function drawTags(tags) {
    let tagContainer = document.querySelector('.tags');
    tags.forEach(item => tagContainer.appendChild( createTag(item) ));
}


function getUniqueTags() {
    let tags = products.reduce((accum, item) => accum.concat(item.tags), ['All']);
    let uniqueTags = Array.from(new Set(tags));
    drawTags(uniqueTags);
}

getUniqueTags();


function createProductCard(product) {
    let productTag = document.createElement('div');
    productTag.classList.add('product');

    let infoTag = document.createElement('div');
    infoTag.classList.add('product-info');

    let nameTag = document.createElement('span');
    nameTag.classList.add('name');
    nameTag.innerText = product.title;

    let priceTag = document.createElement('span');
    priceTag.classList.add('price');
    priceTag.innerText = product.price + '$';

    infoTag.appendChild(nameTag);
    infoTag.appendChild(priceTag);

    productTag.appendChild(infoTag);

    productTag.addEventListener('click', () => {
        createModalInfo(product);
        openModal();
    });

    return productTag;
}

function drawProducts(items) {
    container.innerHTML = '';
    items.forEach(item => container.appendChild(createProductCard(item)));
}

drawProducts(products);

function filterByTag(tagName) {
    if (tagName === 'All') {
        filteredProducts = products;
    } else {
        filteredProducts = products.filter(item => item.tags.includes(tagName));
    }
    drawProducts(filteredProducts);
}

function sortAsc() {
    filteredProducts.sort((a, b) => a.price - b.price);
    drawProducts(filteredProducts);
}

function sortDesc() {
    filteredProducts.sort((a, b) => b.price - a.price);
    drawProducts(filteredProducts);
}

function sortRating() {
    filteredProducts.sort((a, b) => b.rating - a.rating);
    drawProducts(filteredProducts);
}


function openModal() {
    modalWrap.style.display = 'flex';
}

function closeModal() {
    modalWrap.style.display = 'none';
}

function stopProp(event) {
    event.stopPropagation();
}


function createModalInfo(product) {
    let info = document.querySelector('#info');
    info.innerHTML = '';

    let img = document.createElement('img');
    img.src = './images/stub.jpg';
    img.classList.add('modal-img');

    let modalInfo = document.createElement('div');
    modalInfo.classList.add('modal-info');

    let infoName = document.createElement('span');
    infoName.classList.add('info-name');
    infoName.innerText = product.title;

    let ratingProgress = document.createElement('div');
    ratingProgress.classList.add('rating-progress');
    ratingProgress.style.width = product.rating * 20 + '%';

    let rating = document.createElement('div');
    rating.classList.add('rating');

    rating.appendChild(ratingProgress);

    let description = document.createElement('p');
    description.classList.add('info-desc');
    description.innerText = product.description;

    info.appendChild(img);
    
    modalInfo.appendChild(infoName);
    modalInfo.appendChild(rating);
    modalInfo.appendChild(description);

    info.appendChild(modalInfo);
}

function getUniqueSizes() {
    let sizes = products.reduce((accum, item) => accum.concat(item.size), []);
    let uniqueSizes = Array.from(new Set(sizes));
    fillSizeDropdown(uniqueSizes);
}

function createDropdownRow(size) {
    let row = document.createElement('label');
    row.classList.add('area-row');

    let input = document.createElement('input');
    input.type = 'checkbox';
    input.name = size;

    let checkbox = document.createElement('div');
    checkbox.classList.add('checkbox');

    let name = document.createElement('span');
    name.innerText = size;

    row.appendChild(input);
    row.appendChild(checkbox);
    row.appendChild(name);

    return row;
}

function fillSizeDropdown(sizes) {
    let container = document.querySelector('.dropdown-area');
    sizes.forEach(item => container.appendChild(createDropdownRow(item)));
}

getUniqueSizes();


document.querySelector('.dropdown-btn').addEventListener('click', filterProductsBySize);

function filterProductsBySize() {
    let dropdownForm = document.querySelector('.dropdown-area');
    let form = new FormData(dropdownForm);
    let selectedSizes = [];
    for(let item of form.keys()) {
        selectedSizes.push(item);
    }

    if (selectedSizes.length > 0) {
        filteredProducts = filteredProducts.filter(item => {
            let size = selectedSizes.filter(s => item.size.includes(s));
            return size.length > 0;
        });
        drawProducts(filteredProducts);
    }

    document.body.focus();
    
}